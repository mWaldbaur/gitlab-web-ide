import * as vscode from 'vscode';
import { IFileStatus } from '@gitlab/web-ide-fs';
import { GitLabBranch, GitLabProject } from '@gitlab/vscode-mediator-commands';
import {
  COMMIT_COMMAND_ID,
  COMMIT_COMMAND_TEXT,
  SECONDARY_COMMIT_COMMAND_TEXT,
  SOURCE_CONTROL_ID,
  SOURCE_CONTROL_NAME,
  SOURCE_CONTROL_CHANGES_NAME,
  SOURCE_CONTROL_CHANGES_ID,
  FS_SCHEME,
} from '../constants';
import { ResourceDecorationProvider } from './ResourceDecorationProvider';
import { fromUriToScmUri } from './uri';
import { createStatusViewModel, toResourceState } from './status';
import { ISourceControlViewModel } from './types';

interface SourceControlViewModelOptions {
  repoRoot: string;
  sourceControl: vscode.SourceControl;
  changesGroup: vscode.SourceControlResourceGroup;
  resourceDecorationProvider: ResourceDecorationProvider;
}

class SourceControlViewModel implements ISourceControlViewModel {
  readonly #repoRoot: string;

  readonly #sourceControl: vscode.SourceControl;

  readonly #changesGroup: vscode.SourceControlResourceGroup;

  readonly #resourceDecorationProvider: ResourceDecorationProvider;

  #lastStatus: IFileStatus[];

  constructor({
    repoRoot,
    sourceControl,
    changesGroup,
    resourceDecorationProvider,
  }: SourceControlViewModelOptions) {
    this.#repoRoot = repoRoot;
    this.#sourceControl = sourceControl;
    this.#changesGroup = changesGroup;
    this.#resourceDecorationProvider = resourceDecorationProvider;

    this.#lastStatus = [];
  }

  getStatus() {
    return this.#lastStatus;
  }

  getCommitMessage() {
    return this.#sourceControl.inputBox.value;
  }

  update(statuses: IFileStatus[]) {
    this.#lastStatus = statuses;

    const statusVms = statuses.map(x => createStatusViewModel(x, this.#repoRoot));

    // why: It might be important to update the resource decorations first before
    // adding the resources themselves to the resource group.
    this.#resourceDecorationProvider.update(statusVms);
    this.#changesGroup.resourceStates = statusVms.map(statusVm => toResourceState(statusVm));
  }
}

const sourceControlSecondaryCommand = {
  title: SECONDARY_COMMIT_COMMAND_TEXT,
  command: COMMIT_COMMAND_ID,
  arguments: [
    {
      shouldPromptBranchName: true,
    },
  ],
};

const createVSCodeSourceControl = (
  disposables: vscode.Disposable[],
  branch: GitLabBranch,
  project: GitLabProject,
) => {
  const sourceControl = vscode.scm.createSourceControl(SOURCE_CONTROL_ID, SOURCE_CONTROL_NAME);
  const secondaryCommands = project.empty_repo ? undefined : [[sourceControlSecondaryCommand]];
  const commitButtonText = `${COMMIT_COMMAND_TEXT} to '${branch.name}'`;

  disposables.push(sourceControl);

  sourceControl.acceptInputCommand = {
    command: COMMIT_COMMAND_ID,
    title: COMMIT_COMMAND_TEXT,
    arguments: [],
  };

  sourceControl.inputBox.enabled = true;
  sourceControl.inputBox.placeholder = 'Commit message';
  sourceControl.actionButton = {
    description: commitButtonText,
    enabled: true,
    secondaryCommands,
    command: {
      title: commitButtonText,
      command: COMMIT_COMMAND_ID,
    },
  };
  sourceControl.quickDiffProvider = {
    provideOriginalResource(uri: vscode.Uri): vscode.Uri | undefined {
      if (uri.scheme === FS_SCHEME) {
        return fromUriToScmUri(uri, '');
      }

      return undefined;
    },
  };

  return sourceControl;
};

const createVSCodeChangesResourceGroup = (
  disposables: vscode.Disposable[],
  sourceControl: vscode.SourceControl,
) => {
  const changesGroup = sourceControl.createResourceGroup(
    SOURCE_CONTROL_CHANGES_ID,
    SOURCE_CONTROL_CHANGES_NAME,
  );
  changesGroup.hideWhenEmpty = false;
  disposables.push(changesGroup);

  return changesGroup;
};

const createResourceDecorationProvider = (disposables: vscode.Disposable[]) => {
  const resourceDecorationProvider = new ResourceDecorationProvider();

  disposables.push(
    vscode.window.registerFileDecorationProvider(
      resourceDecorationProvider.createVSCodeDecorationProvider(),
    ),
  );

  return resourceDecorationProvider;
};

export const createSourceControlViewModel = (
  disposables: vscode.Disposable[],
  repoRoot: string,
  branch: GitLabBranch,
  project: GitLabProject,
): ISourceControlViewModel => {
  const sourceControl = createVSCodeSourceControl(disposables, branch, project);
  const changesGroup = createVSCodeChangesResourceGroup(disposables, sourceControl);
  const resourceDecorationProvider = createResourceDecorationProvider(disposables);

  return new SourceControlViewModel({
    repoRoot,
    sourceControl,
    changesGroup,
    resourceDecorationProvider,
  });
};

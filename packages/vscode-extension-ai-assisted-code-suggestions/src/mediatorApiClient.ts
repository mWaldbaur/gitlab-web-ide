import { fetchFromApi, COMMAND_FETCH_FROM_API } from '@gitlab/web-ide-interop';
import * as vscode from 'vscode';

export interface MediatorApiClient {
  fetchFromApi: fetchFromApi;
}
// TODO: extract this implementation to a new package
// vscode-mediator-api-client
export const mediatorApiClient: MediatorApiClient = {
  fetchFromApi: async request => vscode.commands.executeCommand(COMMAND_FETCH_FROM_API, request),
};

import * as vscode from 'vscode';
import { COMMAND_GET_CONFIG, InteropConfig } from '@gitlab/web-ide-interop';

export const getConfig: () => Promise<InteropConfig> = async () =>
  vscode.commands.executeCommand(COMMAND_GET_CONFIG);

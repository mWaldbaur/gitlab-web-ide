import vscode from 'vscode';
import { GitLabCodeCompletionProvider } from './completion/GitlabCodeCompletionProvider';
import { createStatusBarItem } from './status/CodeSuggestionStatusBarItem';
import { initializeLogging, log } from './log';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_DISABLE_COMMAND,
  AI_ASSISTED_CODE_SUGGESTIONS_ENABLE_COMMAND,
} from './constants';
import { enableCodeSuggestions, disableCodeSuggestions } from './toggle-setting/toggleSetting';
import { mediatorApiClient } from './mediatorApiClient';
import { getConfig } from './getConfig';
import { convertToGitLabProject, getProject } from './gitlab/api/get_project';

export async function activate(context: vscode.ExtensionContext) {
  const outputChannel = vscode.window.createOutputChannel('AI assisted code suggestions');
  initializeLogging(line => outputChannel.appendLine(line));

  const config = await getConfig();

  const result = await mediatorApiClient.fetchFromApi(getProject(config.projectPath));

  if (!result.project) {
    log.error(`Code suggestions couldn't fetch project ${config.projectPath}`);
    return;
  }

  await GitLabCodeCompletionProvider.registerGitLabCodeCompletion(
    context,
    mediatorApiClient,
    convertToGitLabProject(result.project),
  );

  context.subscriptions.push(
    vscode.commands.registerCommand(
      AI_ASSISTED_CODE_SUGGESTIONS_ENABLE_COMMAND,
      enableCodeSuggestions,
    ),
  );
  context.subscriptions.push(
    vscode.commands.registerCommand(
      AI_ASSISTED_CODE_SUGGESTIONS_DISABLE_COMMAND,
      disableCodeSuggestions,
    ),
  );

  createStatusBarItem(context);
}

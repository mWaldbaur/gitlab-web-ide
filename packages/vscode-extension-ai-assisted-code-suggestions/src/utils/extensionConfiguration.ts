import * as vscode from 'vscode';
import { AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE } from '../constants';

export const AI_ASSISTED_CODE_SUGGESTIONS_MODE = 'gitlab.aiAssistedCodeSuggestions.enabled';
export const AI_ASSISTED_CODE_SUGGESTIONS_CONFIG = 'gitlab.aiAssistedCodeSuggestions';

export interface AiAssistedCodeSuggestionsConfiguration {
  enabled: boolean;
}

export function getAiAssistedCodeSuggestionsConfiguration(): AiAssistedCodeSuggestionsConfiguration {
  const config = vscode.workspace.getConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE);

  return {
    enabled: config.enabled,
  };
}

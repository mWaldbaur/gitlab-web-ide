import * as vscode from 'vscode';
import { disableCodeSuggestions, enableCodeSuggestions } from './toggleSetting';

describe('toggleSetting', () => {
  describe('disableCodeSuggestions', () => {
    const mockUpdate = jest.fn();
    const setting = {
      update: mockUpdate,
    } as unknown as vscode.WorkspaceConfiguration;

    beforeEach(() => {
      jest.mocked(vscode.workspace.getConfiguration).mockReturnValue(setting);
    });

    it('disables the suggestions', async () => {
      await disableCodeSuggestions();
      expect(mockUpdate).toHaveBeenCalledWith('enabled', false, vscode.ConfigurationTarget.Global);
    });

    it('enables the suggestions', async () => {
      await enableCodeSuggestions();
      expect(mockUpdate).toHaveBeenCalledWith('enabled', true, vscode.ConfigurationTarget.Global);
    });
  });
});

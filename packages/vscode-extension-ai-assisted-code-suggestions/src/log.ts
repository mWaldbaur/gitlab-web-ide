import dayjs from 'dayjs';
import { isDetailedError, prettyJson } from './errors';

type logFunction = (line: string) => void;
// eslint-disable-next-line no-console
let globalLog: logFunction = console.error;

export const initializeLogging = (logLine: logFunction): void => {
  globalLog = logLine;
};

interface Log {
  info(e: Error): void;
  info(message: string, e?: Error): void;
  warn(e: Error): void;
  warn(message: string, e?: Error): void;
  error(e: Error): void;
  error(message: string, e?: Error): void;
}

const LOG_LEVEL = {
  INFO: 'info',
  WARNING: 'warning',
  ERROR: 'error',
} as const;

// pad subsequent lines by 4 spaces
const PADDING = 4;

type LogLevel = typeof LOG_LEVEL[keyof typeof LOG_LEVEL];

const multilineLog = (line: string, level: LogLevel): void => {
  const prefix = `${dayjs().format('YYYY-MM-DDTHH:mm:ss:SSS')} [${level}]: `;
  const padNextLines = (text: string) => text.replace(/\n/g, `\n${' '.repeat(PADDING)}`);

  globalLog(`${prefix}${padNextLines(line)}`);
};

const formatError = (e: Error): string =>
  isDetailedError(e) ? prettyJson(e.details) : `${e.message}\n${e.stack}`;

const logWithLevel = (level: LogLevel, a1: Error | string, a2?: Error) => {
  if (typeof a1 === 'string') {
    const errorText = a2 ? `\n${formatError(a2)}` : '';
    multilineLog(`${a1}${errorText}`, level);
  } else {
    multilineLog(formatError(a1), level);
  }
};

const info = (a1: Error | string, a2?: Error) => logWithLevel(LOG_LEVEL.INFO, a1, a2);
const warn = (a1: Error | string, a2?: Error) => logWithLevel(LOG_LEVEL.WARNING, a1, a2);
const error = (a1: Error | string, a2?: Error) => logWithLevel(LOG_LEVEL.ERROR, a1, a2);

export const log: Log = { info, warn, error };

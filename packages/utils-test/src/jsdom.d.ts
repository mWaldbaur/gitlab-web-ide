import type { JSDOM } from 'jsdom';

declare global {
  const dom: JSDOM;
}
